<!DOCTYPE html>
<html>
<head>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <link rel="stylesheet" href="https://192.168.4.106:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://192.168.4.106:8443/assistserver/sdk/web/shared/css/shared-window.css">
    <link href="style.css" type="text/css" rel="stylesheet"/>
</head>

<body>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
<h3>Assist only</h3>
<form class="assisted-session">
    <input type="text" class="shortCode form-control" placeholder="Enter a short-code"><br>
    <button type="submit" class="btn btn-primary">Start</button>
</form>

<h3>Screen share</h3>
<!-- remote screen share -->
<div id="share"></div><!-- request the consumer allow screen share -->
<h3>Mode</h3>
<form class="mode">
    <input type="radio" name="mode" value="control" checked>Control<br>
    <input type="radio" name="mode" value="draw">Draw<br>
    <input type="radio" name="mode" value="spotlight">Spotlight<br>
</form>

<h3>Annotation Control</h3>
<form class="annotation-control">
    <input type="color" class="color" value="#ff0000">
    <input type="text" class="opacity" value="0.5">
    <input type="text" class="width" value="2">
    <button type="submit">Update</button>
    <button id="clear">Clear</button>
</form>
<button type="submit" id="end" class="btn btn-primary">Disconnect</button>


</div>


<!-- libraries needed for Assist SDK -->
<script src="http://192.168.4.106:8080/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>
<script src="http://192.168.4.106:8080/gateway/adapter.js"></script>
<script src="http://192.168.4.106:8080/gateway/csdk-sdk.js"></script>
<script src="http://192.168.4.106:8080/assistserver/sdk/web/shared/js/assist-aed.js"></script>
<script src="http://192.168.4.106:8080/assistserver/sdk/web/shared/js/shared-windows.js"></script>
<script src="http://192.168.4.106:8080/assistserver/sdk/web/agent/js/assist-console.js"></script>
<script src="http://192.168.4.106:8080/assistserver/sdk/web/agent/js/assist-console-callmanager.js"></script>



<!-- load jQuery - helpful for DOM manipulation -->
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- control -->
<script>

    <?php
    // since we know that the agent will require to be provisioned, we
    // can eagerly provision a session token on the page load rather
    // than do so lazily later

    // include the Provisioner class we created to do the provisioning
    require_once('provision.php');
    $provisioner = new Provisioner;
    $token = $provisioner->provisionAgent(
        'agent1',
        '192.168.4.106',
        '.*');

    // decode the sessionID from the provisioning token
    $sessionId = json_decode($token)->sessionid;
    ?>

    var sessionId = '<?php echo $sessionId; ?>';
	// alert(sessionId);
    console.log("SessionID: " + sessionId);

    var AgentModule = function (sessionId) {
        // Declare variables needed during the call
        var
       
            share,
            $annotationControl,
            $mode,
            $assistedSession;


        // organise functions to do important tasks...
        cacheDom(); // cache the DOM elements we will need later during the call
        setClickHandlers();  // define what happens when our app's buttons are clicked
        bindAgentSdkCallbacks();  // set all of the agentSdk's call-back listeners
        linkUi(); //


        // caches the DOM elements we will need later during the call
        function cacheDom() {
            // extract native DOM elements - AgentSDK works with DOM elements directly
            share = $('#share')[0];
            $assistedSession = $('.assisted-session');
            $mode = $('.mode');
            $annotationControl = $('.annotation-control');
        }

        // add click handlers that determine what happens when agent clicks buttons, etc
        function setClickHandlers() {
            

            // handle start-assisted-session being clicked
            $assistedSession.click(function(event) {
                // prevent normal form submission behaviour
                event.preventDefault();

             
                var $this = $(this);
                var shortCode = $this.find('.shortCode').val();

                // Make an AJAX call to the server to resolve the short code to the consumer's full CorrelationID
                $.get('https://192.168.4.106:8443/assistserver/shortcode/agent', {
                    appkey: shortCode
                }, function (config) { // note the call back function through which a config object that contains the CorrelationID is returned to us

                    // Create a settings object that contains the consumer's retrieved CorrelationID, the URL of the Assist server, and the SessionID of the agent's session
                    var settings = {
                        correlationId: config.cid,
                        url: 'https://192.168.4.106:8443',
                        sessionToken: sessionId
                    };

                    // Now start the support session, supplying the consumer's CorrelationID, the URL of the server & your SessionID in form of a settings object
                    console.log('--------- Attempting to start remote session with shortCode: ' + shortCode + ', CID: ' + config.cid + ", and SessionId: " + sessionId);
                    AssistAgentSDK.startSupport(settings);

                    // with the support session started, request sharing of the consumer's screen
                    AssistAgentSDK.requestScreenShare();
                }, 'json');

            });
            $mode.change(function () {
                var $this = $(this);
                var mode = $this.find('[name=mode]:checked').val();
                console.log( '----------- Mode: ' + mode );
                // determine which mode to switch in to
                switch(mode) {
                    case 'control':
                        console.log('--------- controlSelected');
                        AssistAgentSDK.controlSelected();
                        break;
                    case 'draw':
                        console.log('--------- drawSelected');
                        AssistAgentSDK.drawSelected();
                        break;
                    case 'spotlight':
                        console.log('--------- spotlightSelected');
                        AssistAgentSDK.spotlightSelected();
                        break;
                }
            });

            // handle the annocation controls being set
            $annotationControl.submit(function (event) {
                // prevent normal form submission behaviour
                event.preventDefault();

                // extract the vars
                var $this = $(this);
                var color = $this.find('.color').val();
                var opacity = $this.find('.opacity').val();
                var width = $this.find('.width').val();

                // update the annotation draw style
                AssistAgentSDK.setAgentDrawStyle(color, opacity, width);
            });
        }


        // set all of the agentSdk's call-back listeners
        function bindAgentSdkCallbacks() {
            // Connection events
            AssistAgentSDK.setConnectionEstablishedCallback(function () {
                console.log('----------- setConnectionEstablishedCallback');
            });

            AssistAgentSDK.setConnectionLostCallback(function () {
                console.log('----------- setConnectionLostCallback');
            });

            AssistAgentSDK.setConnectionReestablishedCallback(function () {
                console.log('----------- setConnectionReestablishedCallback');
            });

            AssistAgentSDK.setConnectionRetryCallback(function (retryCount, retryTimeInMilliSeconds) {
                console.log('----------- setConnectionRetryCallback');
            });

            // Call event
            AssistAgentSDK.setConsumerEndedSupportCallback(function () {
                console.log('----------- setConsumerEndedSupportCallback');
                // clear the remove video view - optional
                $(remote).find('video').attr('src', '');
            });

            // Assist feature events
            AssistAgentSDK.setFormCallBack(function (callerForm) {
                console.log('---------- setFormCallBack');
                sharedFormContainer.replaceChild(sharedForm, sharedFormContainer.children[0]);
            });

            
			
            AssistAgentSDK.setScreenShareActiveCallback(function (active) {
                console.log('------------ ssetScreenShareActiveCallback active: ' + active);
            });

            AssistAgentSDK.setScreenShareRejectedCallback(function () {
                console.log('------------ setScreenShareRejectedCallback. Caller has rejected agents invitation to screen share');
            });

            AssistAgentSDK.setSnapshotCallBack(function (snapshot) {
                console.log('------------ setSnapshotCallBack');
            });
        }


        // links UI elements to their Agent SDK outlets
        function linkUi() {
            // Set screen share element
            AssistAgentSDK.setRemoteView(share);
        }


    }(sessionId);
	
	
	$('#end').click(function(){
		AssistAgentSDK.endSupport();
	});

</script>
</body>
</html>